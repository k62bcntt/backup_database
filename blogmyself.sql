-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: blogmyself
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `desciption` text COLLATE utf8mb4_unicode_ci,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT 'publish: 1, private: 0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Angular2','angular2',0,'Odio quia est ut et. Soluta sunt quisquam dolorem iste animi est. Voluptates at fugit sit sit. Vel est molestiae hic non dolor. Cupiditate est a cupiditate ea harum eos omnis.',NULL,0,'1','2017-07-23 03:41:09','2017-07-23 03:41:09'),(2,'ReactJs','reactjs',0,'Qui soluta pariatur eos voluptas ad repellendus corporis. Aliquid dolore quasi quia assumenda quia commodi harum. Maxime odit eligendi odio.',NULL,0,'1','2017-07-23 03:41:09','2017-07-23 03:41:09'),(3,'React Native','react-native',0,'Distinctio harum et ut est est illum. Quae labore ab ducimus sit odio saepe repellendus.',NULL,0,'1','2017-07-23 03:41:09','2017-07-23 03:41:09'),(4,'Wordpress','wordpress',0,'Labore in non quia maxime. Voluptatum nulla quo repudiandae odio. Sint magni sunt dolorem quis. In aut ipsa dolorem ut et.',NULL,0,'1','2017-07-23 03:41:09','2017-07-23 03:41:09'),(5,'Server','server',0,'Ut eveniet aspernatur repudiandae dolorem tempora ex fuga. Quas sit vel eum nesciunt. Est nihil tempore id odit. Optio quod aut voluptatibus aliquam quia doloribus velit.',NULL,0,'1','2017-07-23 03:41:09','2017-07-23 03:41:09'),(6,'Html - Css - Jquery','html-css-jquery',0,'Quibusdam ab quidem assumenda sed. Qui quod eligendi itaque quia. Rerum sit tenetur officiis laboriosam ut omnis.',NULL,0,'1','2017-07-23 03:41:09','2017-07-23 03:41:09'),(7,'Server','server',0,'Ut molestiae exercitationem qui quaerat sapiente ullam sed molestiae. Autem ut facere qui magni fuga ipsa. Sit expedita vel voluptatem.',NULL,0,'1','2017-07-23 03:41:09','2017-07-23 03:41:09');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_post`
--

DROP TABLE IF EXISTS `category_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_post_post_id_foreign` (`post_id`),
  KEY `category_post_category_id_foreign` (`category_id`),
  CONSTRAINT `category_post_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `category_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_post`
--

LOCK TABLES `category_post` WRITE;
/*!40000 ALTER TABLE `category_post` DISABLE KEYS */;
INSERT INTO `category_post` VALUES (1,1,2),(2,2,6),(3,3,6),(4,4,6),(5,5,4),(6,6,3),(7,7,6),(8,8,7),(9,9,7),(10,10,5),(11,11,3),(12,12,6),(13,13,1),(14,14,3),(15,15,7),(16,16,2),(17,17,4),(18,18,4),(19,19,2),(20,20,3),(21,21,5),(22,22,1),(23,23,7),(24,24,5),(25,25,2),(26,26,7),(27,27,7),(28,28,3),(29,29,7),(30,30,7);
/*!40000 ALTER TABLE `category_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2015_01_15_105324_create_roles_table',1),(4,'2015_01_15_114412_create_role_user_table',1),(5,'2015_01_26_115212_create_permissions_table',1),(6,'2015_01_26_115523_create_permission_role_table',1),(7,'2015_02_09_132439_create_permission_user_table',1),(8,'2017_06_27_161655_create_posts_table',1),(9,'2017_06_27_161656_create_categories_table',1),(10,'2017_06_27_161657_create_tags_table',1),(11,'2017_06_27_161736_create_postmeta_table',1),(12,'2017_06_27_161840_create_posts_categories_table',1),(13,'2017_06_27_161947_create_posts_tags_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(2,2,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(3,3,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(4,4,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(5,5,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(6,6,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(7,7,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(8,8,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(9,9,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(10,10,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(11,11,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(12,12,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(13,13,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(14,14,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(15,15,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(16,16,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(17,17,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(18,18,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(19,19,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(20,20,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(21,21,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(22,22,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(23,23,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(24,24,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(25,3,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(26,5,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(27,6,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(28,7,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(29,8,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(30,9,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(31,10,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(32,11,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(33,12,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(34,13,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(35,14,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(36,15,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(37,16,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(38,17,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(39,18,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(40,19,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(41,20,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(42,21,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(43,22,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(44,23,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(45,24,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(46,3,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(47,7,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(48,9,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(49,10,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(50,11,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(51,12,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(52,13,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(53,14,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(54,15,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(55,16,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(56,17,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(57,18,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(58,19,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(59,20,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(60,21,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(61,22,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(62,23,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(63,24,3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(64,3,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(65,7,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(66,11,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(67,13,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(68,14,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(69,15,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(70,16,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(71,17,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(72,18,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(73,19,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(74,20,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(75,21,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(76,22,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(77,23,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(78,24,4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(79,3,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(80,7,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(81,11,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(82,13,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(83,14,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(84,15,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(85,17,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(86,18,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(87,19,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(88,21,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(89,22,5,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(90,23,5,'2017-07-23 03:41:07','2017-07-23 03:41:07');
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_user`
--

LOCK TABLES `permission_user` WRITE;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'Create.admin','create.admin','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(2,'Update.admin','update.admin','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(3,'View.admin','view.admin','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(4,'Delete.admin','delete.admin','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(5,'Create.Role','create.role','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(6,'Update.Role','update.role','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(7,'View.Role','view.role','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(8,'Delete.Role','delete.role','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(9,'Create.User','create.user','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(10,'Update.User','update.user','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(11,'View.User','view.user','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(12,'Delete.User','delete.user','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(13,'Create.Post','create.post','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(14,'Update.Post','update.post','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(15,'View.Post','view.post','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(16,'Delete.Post','delete.post','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(17,'Create.Category','create.category','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(18,'Update.Category','update.category','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(19,'View.Category','view.category','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(20,'Delete.Category','delete.category','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(21,'Create.Tag','create.tag','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(22,'Update.Tag','update.tag','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(23,'View.Tag','view.tag','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(24,'Delete.Tag','delete.tag','',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_tag`
--

DROP TABLE IF EXISTS `post_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_tag_post_id_index` (`post_id`),
  KEY `post_tag_tag_id_index` (`tag_id`),
  CONSTRAINT `post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  CONSTRAINT `post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_tag`
--

LOCK TABLES `post_tag` WRITE;
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
INSERT INTO `post_tag` VALUES (1,1,8),(2,1,4),(3,2,1),(4,2,2),(5,2,10),(6,3,9),(7,3,3),(8,4,7),(9,4,5),(10,4,4),(11,5,11),(12,5,2),(13,5,7),(14,6,7),(15,6,9),(16,6,11),(17,7,2),(18,7,3),(19,7,1),(20,8,11),(21,8,7),(22,8,10),(23,9,3),(24,9,9),(25,9,8),(26,10,4),(27,10,11),(28,11,11),(29,11,4),(30,11,3),(31,12,2),(32,12,5),(33,12,7),(34,13,6),(35,13,1),(36,14,4),(37,14,2),(38,14,6),(39,15,8),(40,15,11),(41,15,9),(42,16,10),(43,16,7),(44,17,5),(45,17,10),(46,17,11),(47,18,5),(48,18,3),(49,18,6),(50,19,7),(51,19,4),(52,19,6),(53,20,7),(54,20,8),(55,20,9),(56,21,6),(57,21,4),(58,22,7),(59,22,3),(60,22,9),(61,23,2),(62,23,9),(63,23,11),(64,24,4),(65,24,1),(66,25,2),(67,25,7),(68,25,9),(69,26,3),(70,26,9),(71,27,6),(72,27,3),(73,28,4),(74,28,2),(75,29,6),(76,29,5),(77,29,3),(78,30,8),(79,30,4),(80,30,3);
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `postmeta`
--

DROP TABLE IF EXISTS `postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `postmeta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `meta_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postmeta_post_id_foreign` (`post_id`),
  CONSTRAINT `postmeta_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `postmeta`
--

LOCK TABLES `postmeta` WRITE;
/*!40000 ALTER TABLE `postmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_desciption` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` int(11) NOT NULL,
  `feature` tinyint(1) NOT NULL COMMENT 'Yes: 1, other: 0',
  `order` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT 'public: 2, draft: 1, trash: 0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'Prof.','prof','','Iste autem id distinctio repellat voluptas non eligendi laudantium rerum quam voluptatem omnis repellat laboriosam et ratione ullam incidunt in fugiat id ut qui qui.','Beatae hic sed quo sunt labore. Dolor culpa quibusdam deleniti consequatur est. Veritatis quisquam nihil est perferendis sit non aut. Enim quisquam eos quia aperiam eos voluptas at.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(2,'Mr.','mr','','Nostrum similique accusantium ut perspiciatis quo amet nostrum sit minus voluptas ut possimus quidem distinctio voluptate explicabo rerum amet.','Eum eveniet et labore laudantium dignissimos natus. Ullam optio repudiandae incidunt occaecati quia. Velit magnam doloribus animi libero beatae ipsa aliquam quia.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(3,'Miss','miss','','Cum et nostrum distinctio iusto ut saepe laudantium omnis ut reprehenderit laudantium dicta laboriosam hic facere quidem et accusamus expedita qui.','Sit dolor eius magni veritatis saepe tempore nihil. Modi ut dolor quia laborum quaerat id. Atque amet nulla sit. Sunt eos culpa reprehenderit non in incidunt aut.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(4,'Mr.','mr','','Voluptatem voluptas omnis est reprehenderit in maxime minima eligendi voluptas aut cumque numquam asperiores consequatur fugit incidunt voluptatem et sapiente et velit ipsam id.','Vitae quis consequatur quis autem. Reiciendis voluptatem a placeat. Quis labore asperiores pariatur distinctio fuga. Cum dicta qui perspiciatis tempora ad cumque.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(5,'Prof.','prof','','Expedita molestiae non minima placeat nihil assumenda deleniti nobis sint porro aut non maiores blanditiis qui porro et in sit eligendi.','Officiis a ipsum architecto deleniti explicabo blanditiis alias ducimus. Voluptatem accusamus in expedita. Sint minima quos est nostrum tenetur vel. Accusamus maxime sunt assumenda et provident quam.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(6,'Miss','miss','','Nesciunt accusantium non nisi dignissimos ipsa quaerat officiis porro quos exercitationem error adipisci dolores quia non cumque voluptatem.','Suscipit ullam ex nam suscipit ut eos sit sunt. Ipsa dolorem eaque modi. Dicta perspiciatis libero non aspernatur.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(7,'Dr.','dr','','Quaerat enim nobis quae ut consequatur sit eum nam voluptas et et sit vitae consequuntur odio est ipsum dolores ipsa.','Ab eveniet voluptatem rerum dolor. Ea ut sed temporibus rerum. Temporibus et ab commodi repellendus repudiandae aut officiis. Sit maiores cumque aliquam consequatur.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(8,'Mr.','mr','','Quisquam sit praesentium porro temporibus repudiandae quaerat error labore laudantium doloribus molestias vel molestiae occaecati id occaecati quo.','Aut incidunt sed laborum blanditiis cumque aspernatur excepturi. Non impedit aperiam itaque.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(9,'Prof.','prof','','Et ab sapiente ad dolor vel doloribus aut nam velit quas et est nobis magnam tenetur voluptatum itaque quis iusto iste sapiente sed dolorum molestiae maiores repellendus.','Rerum omnis quis iste similique quasi magni soluta. In beatae id cum reiciendis dolorum qui. Et est ut consequatur totam voluptas molestiae debitis. Et animi modi nisi.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(10,'Prof.','prof','','Sed ipsam rerum laboriosam tenetur rerum non explicabo et porro est aut quod quia commodi id veniam ut vero enim debitis totam natus ratione et illum alias porro.','Dolor alias iure culpa ducimus aut. Vel qui est nesciunt maiores optio ab cupiditate. Commodi vitae id natus et voluptate odit. Dolore deserunt necessitatibus modi minus rerum fugiat voluptas.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(11,'Miss','miss','','Voluptatem nobis ut est vel omnis ut tempora voluptatibus rem quibusdam eos numquam repellat excepturi ullam doloremque.','Harum vel aliquid cupiditate autem corporis et sed. Doloribus minus rerum minima omnis omnis minima.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(12,'Ms.','ms','','Eum dicta ipsa ducimus quod repellendus excepturi quas quo aut laudantium et quo laudantium tenetur necessitatibus laborum ipsam blanditiis nisi.','Officiis neque ut fugit. Et dolores praesentium nihil maiores. Natus eos reprehenderit alias placeat. Ad voluptatem quia quaerat ea voluptas.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(13,'Prof.','prof','','Et ipsam ullam maiores inventore maiores totam aut voluptatem aliquid tempore ex odio aliquid assumenda.','Sapiente ullam dolore accusantium at cum rerum sit. Quia nulla sed ab aspernatur. Nobis alias voluptatibus qui nemo.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(14,'Dr.','dr','','Minus consequatur nihil excepturi deleniti fugiat dolore soluta dolor exercitationem culpa quia quos expedita repellat.','Nesciunt vel sunt dolorum voluptatem placeat. Eos non quia facere omnis. Consequuntur perferendis libero sapiente aut ut deleniti.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(15,'Miss','miss','','Accusamus voluptatem et et quidem commodi magnam voluptatem et vel et neque autem iure quis et beatae accusantium distinctio fugiat vel.','Vel natus eius atque cupiditate. Quibusdam et voluptatem eligendi ipsa provident. Sunt est minus quis earum quas.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(16,'Mrs.','mrs','','Aut a qui voluptate consequuntur velit doloremque ea facilis dolorum sint tenetur laboriosam eveniet eum sapiente vero nemo incidunt et aut.','Cumque repudiandae vel error. Non modi ipsa facilis et ratione veritatis nam. Eum dolores eum explicabo eos. Autem veniam est cum minus quae.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(17,'Dr.','dr','','Officia accusantium eligendi et provident est officia nihil similique eligendi culpa iure neque reprehenderit quia inventore velit et ipsum aperiam assumenda eos officiis odit non eligendi commodi harum corporis.','Excepturi ab laudantium atque nulla eos ipsam. Qui est laudantium nam dolorem et incidunt. Est officia qui officia optio sed aliquid. Non qui ut veniam et veritatis eveniet doloribus excepturi.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(18,'Prof.','prof','','Aut quis voluptates ut quia non praesentium esse vero eum fuga ab magni consequatur adipisci ipsum pariatur provident et.','Quam doloribus rerum vel ratione pariatur quidem. Eum recusandae id et vero.\nEa et eos quia. Autem cumque dignissimos necessitatibus sed sit dolores aut. Qui inventore ex est laboriosam non quidem.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(19,'Dr.','dr','','Ratione tempore dolorem vel esse enim eaque et quasi et culpa similique doloribus qui recusandae a veritatis.','Debitis id voluptatem tempore molestiae et. Eos repudiandae aut assumenda non aut. Sit consequatur reprehenderit et voluptatibus optio voluptatem.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(20,'Miss','miss','','Culpa sint nesciunt est quam est doloribus deleniti omnis labore quis ea ut numquam deleniti omnis vitae totam.','Et voluptate repudiandae aliquid eos maiores et optio. Delectus aperiam ratione atque sequi cupiditate commodi ut. Vel minima doloremque et quo tempore est.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(21,'Miss','miss','','Consequatur enim id et voluptatem atque dicta voluptatem in eligendi delectus inventore voluptatem.','Delectus consequatur commodi nesciunt. Quis dolorem numquam et expedita ipsam culpa. Inventore quidem quaerat ut aperiam tempora reiciendis nam corrupti.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(22,'Mrs.','mrs','','Non magnam rerum et consequatur explicabo aut perferendis dolorem culpa vero odit magni aliquam.','Porro culpa ex laudantium eos et quia perferendis. Possimus odit occaecati exercitationem ea quis accusantium incidunt. Quasi eaque earum ab quae provident sit neque.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(23,'Mr.','mr','','Aut nobis maxime facere enim aut nesciunt aut nostrum voluptatum est et ut iure quia inventore sed quae qui doloribus doloremque aut nihil voluptas tenetur qui quis.','Dolores qui vel qui est aut. Delectus id non non quia enim. Tempore quas non ut consectetur. Repellendus totam accusamus accusantium iure in quis placeat.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(24,'Miss','miss','','Corporis doloremque et rem totam error nobis voluptates ut est quia qui quasi quos ullam reiciendis voluptatibus alias non voluptatem aspernatur ratione accusamus.','Laudantium dolore sed cupiditate sunt. Totam dolores nobis laboriosam soluta quasi debitis aliquid. Odio pariatur quia ducimus temporibus. Vel voluptatem ea adipisci laborum voluptatem eum cumque et.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(25,'Mrs.','mrs','','Explicabo sit sint nihil sed rerum minus optio est sequi accusamus quo laboriosam non aut est.','Alias eos velit qui error. Sed saepe qui eos autem excepturi ex mollitia. Ut sit dolores dolores nisi sit. Earum placeat voluptates voluptatem itaque deserunt excepturi est.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(26,'Prof.','prof','','Harum aut qui voluptas porro voluptates hic nobis autem hic earum tenetur impedit autem reprehenderit rerum qui eum debitis voluptas ut reiciendis expedita.','Temporibus dolore facere autem est. Voluptas enim dolores ut. Officiis debitis voluptatem aut aut dignissimos. Dolorem veritatis alias quae molestiae voluptas eaque voluptatum.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(27,'Dr.','dr','','Natus ratione eum enim saepe placeat voluptas labore corrupti laudantium sint ipsam beatae eum cum eius consequatur ab ab aut quo sit.','Totam modi qui aut accusantium enim. Natus omnis est ut ut omnis officia vel quis. Ut quia repellendus beatae. Molestias ipsa quia maiores distinctio ea incidunt.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(28,'Dr.','dr','','Commodi inventore asperiores ad eaque possimus in et molestiae repudiandae quis fugit fugiat unde inventore et hic eum quia beatae cupiditate et maiores beatae.','Nam molestias est in odio sit deleniti totam. Iste voluptatum molestias molestiae culpa. Dolores vel praesentium vitae.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(29,'Dr.','dr','','Libero dolor eos est distinctio natus iusto voluptas est quo provident aut ullam incidunt deserunt ducimus ut dolore aut corporis distinctio molestiae sit et quia aut libero.','Et aliquid nobis nihil commodi aliquam totam. Eum ut eum suscipit. Est quis quidem quos omnis modi perferendis libero dolorum.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(30,'Mr.','mr','','Ratione eius laudantium accusantium error dolorem unde qui et doloribus adipisci non voluptatibus ea explicabo suscipit dicta eos dolorem odit unde aliquid ad voluptas voluptatem vitae eius.','Aut et deserunt rem quibusdam. Doloremque facilis asperiores recusandae a perferendis doloribus laudantium. Consequuntur unde culpa eligendi.','post',1,0,0,2,'2017-07-23 03:41:09','2017-07-23 03:41:09');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(2,2,2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(3,3,3,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(4,4,4,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(5,5,5,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(6,5,6,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(7,5,7,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(8,5,8,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(9,5,9,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(10,5,10,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(11,5,11,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(12,5,12,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(13,5,13,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(14,5,14,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(15,5,15,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(16,5,16,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(17,5,17,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(18,5,18,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(19,5,19,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(20,5,20,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(21,5,21,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(22,5,22,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(23,5,23,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(24,5,24,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(25,5,36,'2017-08-05 02:57:52','2017-08-05 02:57:52'),(26,5,37,'2017-08-05 02:58:35','2017-08-05 02:58:35'),(27,5,38,'2017-08-05 03:35:10','2017-08-05 03:35:10');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'SuperAdmin','superadmin','',1,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(2,'Administrator','admin','',2,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(3,'Manager','manager','',3,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(4,'Moderator','moderator','',4,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(5,'User','user','',5,'2017-07-23 03:41:07','2017-07-23 03:41:07');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Public: 1, Private: 0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'HTML','html',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(2,'CSS','css',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(3,'React Native','react-native',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(4,'Wordpress','wordpress',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(5,'Server','server',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(6,'JQuery','jquery',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(7,'Plugin','plugin',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(8,'Lập trình','lap-trinh',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(9,'Ngôn ngữ','ngon-ngu',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(10,'SEO','seo',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(11,'Website','website',0,1,'2017-07-23 03:41:09','2017-07-23 03:41:09');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:male , 1:female',
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Super Admin','Super Admin','superadmin@gmail.com','$2y$10$21c9.UZvzMHnZyft6FSjI.cpbvfpqyNFl1SHeXe6JzJLj5m4OhhrK',1,'693-830-7496 x9902','1971-01-27','33333 Champlin Ranch\nSpinkatown, NJ 59145','0',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(2,'Admin','Admin','admin@gmail.com','$2y$10$qZ6sjT4BB9nGH1LnyZ37..g90OnpgFFGGcFhnH2bCxH2lkq3894Xa',0,'1-457-975-9833','1992-01-24','30927 Hettinger Coves\nAlbinbury, OH 91974-0274','0',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(3,'Manager','Manager','manager@gmail.com','$2y$10$OtuESVM210Wd35SvAVaDH.p1zk7ZInptRWgf/LfaTkXEPecR2yAlu',1,'884.675.1281 x8585','1998-09-22','4805 Leonardo Points Suite 915\nNew Lucyberg, ND 80943','0',NULL,'2017-07-23 03:41:07','2017-07-23 03:41:07'),(4,'Moderator','Moderator','moderator@gmail.com','$2y$10$yTJhgrpjYfKalQna6vODI.pt/LPXLO8cKrxTaP0wSDuUChZh7/TRi',1,'542.319.9354 x363','1978-05-05','473 Kavon Dam\nKimview, NJ 19273-5581','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(5,'Kendall Littel','Kendall Littel','andy98@gmail.com','$2y$10$HlIDBVFpz/qX/pkh4PuxCemZcuVUzGRwoXaJakFx0f6X7xUVqPbMW',0,'402-696-9076','1974-12-07','92372 Swift Alley\nNew Edwinafort, WV 77092','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(6,'Sabina Schneider','Sabina Schneider','jenkins.jonas@hotmail.com','$2y$10$wHfP3VBpqrz4O5RTXh4Wuu5je6IM7zCNL.GeQffUIWmFOj5tAwIqe',1,'885.653.3831 x808','1995-01-27','910 Hegmann Club Suite 759\nNew Wardmouth, NE 48019-9282','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(7,'Retha Gottlieb','Retha Gottlieb','mcremin@mosciski.com','$2y$10$UimpgAhwX1u1vvCM7gA7o.noGEIVSIWxbLw/ebsreROro0Y32dXzm',1,'(660) 393-6246','1976-01-23','763 Pearline Forest\nPierceview, WY 39155-5700','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(8,'Ansley Cremin','Ansley Cremin','kihn.preston@yahoo.com','$2y$10$JbB556xGRy3jOxdx9bXPKe49luhKwguPYDjWLlm4xowYsnbTryKLy',0,'448-742-4789 x632','1984-08-11','960 Abbott Extensions\nBrandynberg, MI 66059','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(9,'Alena McCullough','Alena McCullough','pwhite@beer.com','$2y$10$A4SH0.j6lP7WuHxzGrlFUOaFxKMZJyFcBoU1KlfzxeqmjqS4DAKEa',1,'+1-489-932-2789','1984-12-05','7236 Skye Forges Apt. 621\nLemkestad, SC 40772-2268','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(10,'Lamar O\'Conner','Lamar O\'Conner','mueller.bianka@hotmail.com','$2y$10$xWiOffr5M0ud55KHY1tUvOUeY4iXO4fk5Y78MF2cEafzrkfdYe1GK',0,'1-964-691-1318 x885','1991-11-05','414 Rosa Ridge Apt. 988\nNew Terrillside, CT 68500','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(11,'Therese Lebsack','Therese Lebsack','elangosh@yahoo.com','$2y$10$d810g3xljdZqv.9mHDKev.Lt2xMBYA/OgjK53Y3ktxY6MvY9EqV8u',1,'+16088817182','1980-03-21','742 Julio Falls Apt. 729\nWest Martin, TX 49107-5546','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(12,'Gregory Heller','Gregory Heller','yvette10@gmail.com','$2y$10$AUeqwveGZ9san0QMhD.oDeochhuav1o1OdANuYtIpcdNXkKcXQU56',0,'+1-857-292-4947','1995-01-17','440 Deangelo Street\nWest Florine, MO 08275','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(13,'Vergie Olson','Vergie Olson','halvorson.clemens@corwin.com','$2y$10$TPxD4YbdDsMSGvDdKe5zWu7V6o60vDdata.8rmaQ6/sw3obDXAHte',1,'947-905-1997 x449','1986-12-12','72568 Murphy Corner\nSouth Stefanie, WV 66418-3970','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(14,'Kaelyn Feeney','Kaelyn Feeney','liza70@yahoo.com','$2y$10$vqG7fy0nXUrK8SPmRv0eZuR.nsjfW.KaUSeh0W3QUJU7ptEtT8sGi',1,'1-531-783-8476 x14647','1992-02-22','923 Roma Wells\nEldaborough, MS 02723-2363','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(15,'Tavares Graham','Tavares Graham','morissette.florida@yost.org','$2y$10$dky.QeejcHj9x7tB9wH1MOXk8jEouWplp9cHmkYpfE40k9ZYvvoAK',0,'896.984.3517 x5953','1992-10-20','349 Hills Ridge\nGraysonland, ID 15960','0',NULL,'2017-07-23 03:41:08','2017-07-23 03:41:08'),(16,'Savanna Krajcik','Savanna Krajcik','maximilian.gerlach@ratke.info','$2y$10$nkSgG9VQUzVPr7Pq8WBz6OPIMNe9F.vIN8zzU3fljAnZ.a.YkeAGu',1,'801-908-8910','2002-01-02','858 Cremin Expressway\nPort Reyna, IL 11279-2970','0',NULL,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(17,'Christelle Ondricka','Christelle Ondricka','jruecker@gmail.com','$2y$10$v7J3rBITq9A23yppygkJKeqT1YzxH6wM5JGXcuLSOM0goMm30CIvi',1,'(656) 762-1898','1998-08-11','592 Katrine Drive\nNew Domenicaland, WY 82459-1113','0',NULL,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(18,'Payton Sawayn','Payton Sawayn','cassin.alysha@yahoo.com','$2y$10$oSe37MtUkqyWBA.zuojMPe6SSgsJAZlTMBVHO9ScqHNkTfYZNlh96',0,'1-536-963-1841 x093','1988-06-15','46036 O\'Kon Stream Apt. 051\nEast Gennaroview, DE 69091','0',NULL,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(19,'Carmela Beer','Carmela Beer','stacey.durgan@yahoo.com','$2y$10$svWDgU9P3bgZAQK5yUUFBO1WdEEvpsiFw4OMnN28uvo75BcOuZKXi',1,'(810) 443-4000','1989-02-06','398 Cortney Street Apt. 909\nZolaton, DC 37501','0',NULL,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(20,'Max Buckridge','Max Buckridge','ansel.nitzsche@botsford.com','$2y$10$Qq9Ar1waS9D9x1ZMSnTKYeApf8KbH3gNsP0jEqUf/dsCnGjNwCuna',0,'+1-637-479-0242','1973-12-15','918 Lemke Grove\nBartellfort, UT 41250','0',NULL,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(21,'Ambrose Gutmann','Ambrose Gutmann','rjacobi@yahoo.com','$2y$10$ijLQR2nBjdru58v3HnL0IuYSrmJWfeDlm21YWyFw1esYXPLqTwDOO',0,'1-863-518-6964','1975-12-30','8014 Amelie Prairie\nAndrewton, MT 25290-9877','0',NULL,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(22,'Estelle Koss','Estelle Koss','loyce99@gmail.com','$2y$10$gAJ4uVqLHTmoVrXbrWvPvur4ajmDNeQYmPPIjozA.Dx3YwoWPP5ta',1,'632-295-3816 x7160','1990-05-04','81120 Feeney Ridges Apt. 106\nWest Jordanestad, DE 03673','0',NULL,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(23,'Fabian Koss','Fabian Koss','crist.rubye@bruen.com','$2y$10$F19SIXL4I7HbL.2uqPaEk.3XNOIdcz9moQPN86X9xcu5qTRYTsqPW',0,'270.990.6710 x7017','1994-01-11','9351 Emmet Island\nKossside, LA 83211','0',NULL,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(24,'Otilia Tromp','Otilia Tromp','tnicolas@yahoo.com','$2y$10$SrmL5Uefyw67dwW3ETPyouFGHgZpmI51/1InQ5yYnF.L.LJKnxRXO',1,'(491) 688-1655','1981-12-07','71479 Treutel Mall\nGradyhaven, KS 06478-6643','0',NULL,'2017-07-23 03:41:09','2017-07-23 03:41:09'),(25,'Quy Dau','Garung','daudq.info@gmail.com','123456',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-07-30 16:46:48','2017-07-30 16:46:48'),(27,'Quy Dau','Garung','daudq.info1@gmail.com','$2y$10$S38BsW1Otm9XwM4zhmIrDOujfe3Hf1l8GcttWNL3NxD1uAeuYZggi',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-07-30 16:50:26','2017-07-30 16:50:26'),(28,'Quy Dau','Garung','daudq.info2@gmail.com','$2y$10$Rd5GlPr1hw1J30O/qqxsO.VKWC6EyXmOgHSi9gOU.MSNg1lH.FVsu',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-08-05 02:44:43','2017-08-05 02:44:43'),(30,'Quy Dau','Garung','daudq.info3@gmail.com','$2y$10$Ljw7Ox31p.4uidW9K4STIu6/Mf1V2W4fumf5arx4cMggvBRoSvCFC',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-08-05 02:52:23','2017-08-05 02:52:23'),(31,'Quy Dau','Garung','daudq.info4@gmail.com','$2y$10$BkjDXi3UzObPugcAdybk4ujrM9kvbzFDcdlymHJp2UG89Ukr25Xo6',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-08-05 02:54:40','2017-08-05 02:54:40'),(32,'Quy Dau','Garung','daudq.info5@gmail.com','$2y$10$Nvl0qMx6FHApqQVOcRSpeeykIOKl8X4P0eugB..J1vVkRjt8Hc14C',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-08-05 02:55:26','2017-08-05 02:55:26'),(33,'Quy Dau','Garung','daudq.info6@gmail.com','$2y$10$Gs2MOnrK3GjQKtuStawOu.M0QGQNSc7/NkI5ynMRUkJ0MysKAYmU6',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-08-05 02:56:04','2017-08-05 02:56:04'),(34,'Quy Dau','Garung','daudq.info7@gmail.com','$2y$10$OyTkEmY0ODlPGObiCBFoaux2eTb9NkhXRkkRN8eSlpw0l/mQj6OSq',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-08-05 02:56:32','2017-08-05 02:56:32'),(35,'Quy Dau','Garung','daudq.info8@gmail.com','$2y$10$2h9cT2FdXxOdlfjJNpbNbeefFshIuoKXqHQb3PtJDmJIKZ1KJfr5S',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-08-05 02:57:25','2017-08-05 02:57:25'),(36,'Quy Dau','Garung','daudq.info9@gmail.com','$2y$10$Ln/IUAI2eB579aW5itKhbunc0g11vgwfWhELr1afrWh2G4.9sdoMq',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-08-05 02:57:52','2017-08-05 02:57:52'),(37,'Quy Dau','Garung','daudq.info11@gmail.com','$2y$10$a4A9q5.OhVe9MykYbpF5W.PJg3LsX75aRdCKOAhSy03hrF3xTGuvi',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-08-05 02:58:35','2017-08-05 02:58:35'),(38,'Quy Dau','Garung','daudq.info10@gmail.com','$2y$10$yaIQRI2CigeiVA7rq0zqWu26L8Y22mjZWxn4J.ECkntspfyy/ne3y',0,NULL,'1993-01-01','abc, def','0',NULL,'2017-08-05 03:35:10','2017-08-05 03:35:10');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-10  5:46:54
